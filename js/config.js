// JavaScript Document

var Page = Class.create();  
Page.prototype = {  
    //初始化  
    Init:function(options){  
	    this.Document = document;
		this.Window = window;
        this.SetOptions(options);//设置参数  
          
        this.Pextpage=this.Options.prevpage;  
        this.Nextpage=this.Options.nextpage;  
       
		this._pageIndex = 0;//当前页
    },  
	
	//设置参数  
    SetOptions:function(options){  
        this.Options={  
            prevpage:"0",        //前一页  
            nextpage:"0",       //后一页  
			
		
        };  
        Extend(this.Options,options||{});  
    },  
};  

var pageInstance=[];
pageInstance[0] = new Page({prevpage:"teatype",nextpage:"teatype"});
pageInstance[1] = new Page({prevpage:"tealist",nextpage:"order"});

